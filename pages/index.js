import React from "react";
import Head from "next/head";

// Components
import Layout from "../components/Layout";
import Profile from "../components/Profile";
import About from "../components/About";
import SocialMedia from "../components/SocialMedia";

// Google Analytics
import { initGA, logPageView } from "../utils/analiytics";

export default class Index extends React.Component {
  componentDidMount() {
    if (!window.GA_INITIALIZED) {
      initGA();
      window.GA_INITIALIZED = true;
    }
    logPageView();
  }
  render() {
    return (
      <Layout>
        <Head>
          <title>Oğuzhan İNCE | Frontend Developer 😇</title>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <meta name="author" content="Oguzhan INCE" />
          <meta name="description" content="Oguzhan INCE personal web site" />
          <meta
            name="keywords"
            content="oguzhan,ince,web,frontend,design,mobile,software"
          />
          <meta name="robots" content="index, follow" />
          <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
          <meta name="language" content="Turkish" />
          <meta
            property="og:title"
            content="Oğuzhan İNCE | Frontend Developer"
          />
          <meta property="og:description" content="Kişisel Web Sayfası" />
          <meta
            property="og:site_name"
            content="Oguzhan INCE | Frontend Developer"
          />
          <meta name="twitter:card" content="summary_large_image" />>
          <meta name="twitter:site" content="@oguz_sh" />>
          <meta name="twitter:creator" content="oguz_sh" />>
          <meta
            name="twitter:title"
            content="Oğuzhan İnce | Frontend Developer"
          />
          <meta name="twitter:description" content="Kişisel Web Sitesi" />>
          <link rel="icon" type="image/png" href="/favicon.png"></link>
        </Head>
        <Profile />
        <About />
        <SocialMedia />
      </Layout>
    );
  }
}
