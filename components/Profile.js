import React from "react";

const profileStyle = {
  width: 300,
  height: 300,
  background: 'url("/profile.jpeg") no-repeat center',
  backgroundSize: "300px 400px",
  borderRadius: "50%",
  boxShadow: "0px 0px 15px rgba(0, 0, 0, 0.5)"
};

const Profile = () => <img style={profileStyle} />;

export default Profile;
