import React from "react";

const layoutStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  margin: 20,
  height: "90vh",
  fontFamily: "Roboto",
  textAlign: "center"
};

const Layout = props => (
  <div style={layoutStyle}>
    <main>{props.children}</main>
  </div>
);

export default Layout;
