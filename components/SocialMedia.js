import React from "react";

const socialMediaWrapper = {
  display: "flex",
  justifyContent: "center"
};

const socialMediaIcon = {
  width: "24px",
  height: "24px",
  margin: 8
};

const SocialMedia = () => {
  return (
    <div style={socialMediaWrapper}>
      <a href="https://twitter.com/oguz_sh" target="_blank">
        <img
          src="/icons/twitter.svg"
          style={socialMediaIcon}
          alt="Twitter"
        ></img>
      </a>
      <a href="https://instagram.com/oguz.sh" target="_blank">
        <img
          src="/icons/instagram.svg"
          style={socialMediaIcon}
          alt="Instagram"
        ></img>
      </a>
      <a href="https://www.linkedin.com/in/oguzhanince/" target="_blank">
        <img
          src="/icons/linkedin.svg"
          style={socialMediaIcon}
          alt="LinkedIN"
        ></img>
      </a>
      <a href="https://codepen.io/0guzhan" target="_blank">
        <img
          src="/icons/codepen.svg"
          style={socialMediaIcon}
          alt="Codepen"
        ></img>
      </a>
    </div>
  );
};

export default SocialMedia;
