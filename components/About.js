import React from "react";

const headerStyle = {
  color: "#1D1D1D",
  letterSpacing: 1.2
};

const paragraphStyle = {
  lineHeight: 1.8,
  width: 500,
  fontSize: 16,
  color: "#1D1D1D"
};

const linkStyle = {
  overflow: "none",
  fontWeight: "bold",
  textDecoration: "none",
  padding: 2,
  margin: 2,
  outline: "none",
  transition: "0.2s ease-in-out"
};

const About = () => (
  <div>
    <h1 style={headerStyle}>Hi, I'm Oguzhan</h1>
    <p style={paragraphStyle} className="mobile-p">
      I'm an enthusiastic frontend developer and a researcher. I
      <a
        href="https://www.github.com/oguzsh"
        style={linkStyle}
        className="yellow"
        target="_blank"
      >
        code
      </a>
      and
      <a
        href="https://www.behance.net/oguzsh"
        style={linkStyle}
        className="pink"
        target="_blank"
      >
        design
      </a>
      ideas. Also, I
      <a
        href="https://medium.com/@oguzsh"
        target="_blank"
        style={linkStyle}
        className="blue"
      >
        write
      </a>
      Turkish articles about psychology and software. If you wanna say "Hi",
      you'll click
      <a href="mailto:oguzhan824@gmail.com" style={linkStyle} className="green">
        here.
      </a>
    </p>
    <style jsx>{`
      .yellow {
        color: #fdbd0a;
      }
      .yellow:hover {
        border: 2px solid #fdbd0a;
      }
      .pink {
        color: #ff00f7;
      }
      .pink:hover {
        border: 2px solid #ff00f7;
      }
      .blue {
        color: #405ef0;
      }
      .blue:hover {
        border: 2px solid #405ef0;
      }
      .green {
        color: #11d61e;
      }
      .green:hover {
        border: 2px solid #11d61e;
      }

      @media only screen and (max-width: 600px) {
        .mobile-p {
          width: 100% !important;
        }
      }
    `}</style>
  </div>
);

export default About;
